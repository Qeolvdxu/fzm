#!/bin/sh

# Debug args
# echo $@

# Print usage if no other arugments are given
if [ $# -eq 0 -o "$1" = '-h' -o "$1" = '--help' ]
then
echo "USAGE: 
	search for and modify a topic in any category:
		fzm -a
	create category or list entries in that category:
		fzm [category]
	create or modify a topic:
		fzm [category] [topic]
	merge two topics or categories together (symlinks the second to the first):
		fzm -m 
	delete a topic:
		fzm [category] [topic] -d
	delete a category
		fzm [category] -d

"
	exit 0
fi

# Initialize
PREFIX=$HOME/.fzm
if [ -d "$PREFIX"  ]
then
echo Reading  db at $PREFIX
else
echo "Initializing fzm db at $PREFIX"
mkdir -p $PREFIX
fi

# Prompt to delete
# $1 = message
# Returns 0 if Y and 1 if N
YN(){
while true; do
	read -p "$1" yn
	case $yn in
		[Yy]* ) return 0; break;;
		[Nn]* ) return 1; break;;
    esac
done

}

# Handles Topic creation under a given category
Topic(){
	# Verify category exists
	filepath="$PREFIX/$1"
	if [ ! -d "$filepath" ]
	then
		echo Missing Category $1
		exit 1
	fi

	# If Topic file exists
	filepath="$PREFIX/$1/$2" 
	if [ ! -f "$filepath" ]
	then
		echo  "$filepath" > $filepath
		echo  "=================" >> $filepath
		echo  "Tags" >> $filepath
		echo  "=================" >> $filepath
		echo  "$1               " >> $filepath
		echo  "                 " >> $filepath
		echo  "=================" >> $filepath
		
	fi
	"${EDITOR:-vim}" "$filepath"
}

# Select a Topic from a Category
# if $1 is not specified, all topics are chosen.
SelectTopicFromCategory(){
	filepath=$(find  "$PREFIX/$1" | fzf)
		echo $filepath
		# If the path is the same as the prefix, do nothing
		if [ "$filepath" = "" -o "$filepath" = "$PREFIX/$1" ]
		then
			 echo No Topic Chosen. Exiting.
			 return
		fi
		"${EDITOR:-vim}" "$filepath"
}


# Handles Category Creation and viewing
Category(){
if [ -d "$PREFIX/$1" ] 
	# Let the user choose which topic they want to use
	then 
		SelectTopicFromCategory $1
	# Create a new category
	else
		echo Creating category "$PREFIX/$1"
		mkdir -p "$PREFIX/$1"
	fi

}

DeleteTopic(){
	# If Topic file exists
	filepath="$PREFIX/$1/$2" 
	if [ -f "$filepath" ]
	then
		YN "Are you sure you want to delete topic $1/$2? [Y/n] " 
		if [ $? -eq 0 ]; then
			echo "Deleting $filepath" 
			rm $filepath; 
		else
			echo Canceling
		fi
	# Topic Didn't exist
	else
		echo "Topic $1/$2 not found."	
	fi
}

DeleteCategory(){
	echo Deleting category $1 and all its topics
	echo Topics:
	echo $(ls $PREFIX/$1)
	echo
	YN "Are you sure you want to continue? [Y/n] "
	if [ $? -ne 0 ]; then
		 return
	 fi
	YN "This is the last confirmation. This data cannot be recovered. Are you sure you want to delete this category? [Y/n] "
	if [ $? -eq 0 ]; then
		echo "Deleting $PREFIX/$1"
		rm -rf $PREFIX/$1
	fi
}

# Runs fuzzy finder on the folder specified by $1.
# Set the env variable FZM_HEADER before calling this to give fuzzySelect a text header
function fuzzySelect(){

	[ -z "$FZM_HEADER" ] && FZM_HEADER=""
	filepath=$(find  "$PREFIX/$1" | fzf --header="$FZM_HEADER")
	# If the path is the same as the prefix, return nothing
	if [ "$filepath" = "" -o "$filepath" = "$PREFIX/$1" ]
	then
		 return
	fi
	echo $filepath
}

# Merges two topics together
Merge(){
	export FZM_HEADER="Choose the first file."
	path1=$(fuzzySelect)
	echo "Selected $path1"
	export FZM_HEADER="Choose the second file."
	path2=$(fuzzySelect)
	echo "Selected $path2"
	# Both categories
	if  [ -d "$path1" ] && [  -d "$path2" ] 
	then
		echo Merge Category "$path2" into "$path1" ?
	# Both topics
	elif [ -f "$path1" ] && [ -f "$path2" ]
	then
		echo Merge Topic "$path2" into "$path1" ?
	# One is a category and one is a topic
	elif [ -f "$path1"  ]
	then
		echo Cannot merge category "$path2" into topic "$path1".
	else
		echo Cannot merge topic "$path2" into category "$path1".
	fi

}

# Function based on argument select
case $# in
	# category format
	1) 
		if [ $1 = '-a' ] 
		then
			# Show all categories
			Category
		elif [ $1 = '-m' ]
		then
			Merge
		else
			# Pull up a specific category.
			Category $1
		fi

	;;
	# category topic format
	2)
		if [ $2 = '-d' ]; then
			DeleteCategory $1	
		else
			Topic $1 $2
		fi
	;;
	# category topic -flag format
	3) 
		if [ $3 = '-d' ] ;then
			DeleteTopic $1 $2		
		fi
esac


