# Fuzzy Manual (fzm)
Fuzzy Manual is a small light weight note taking app.
The main usage for this app is to organize small bits of advice about many different topics,
or as a knowledge base for solving IT problems.

# Installation
Install fuzzy finder (fzf).
Install vim or set the EDITOR variable to your text editor.
To install fzm, clone this repository and run the following command (root permissions may be required)
```
ln -s PATH fzm
```
where PATH is some folder inside your $PATH environment variable.
This creates a symbolic link to the version from this repository, allowing you to update via git pull.

# Removal
To uninstall fzm, simple remove fzm from the path you installed it.
You can use the `which` command to find it.

To remove all files associated with fzm, remove all files in the `.fzm` folder in your home directory.

# Usage
Type `fzm --help` to see usage.
